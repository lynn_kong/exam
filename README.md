# 在线考试
[演示地址](http://exam.wcpdoc.cn)

## 前台

> ###首页
> 
![mashua](http://exam.wcpdoc.cn/img/1.png)

> ###导航页
>
![mashua](http://exam.wcpdoc.cn/img/2.png)
> ###考试
>
![mashua](http://exam.wcpdoc.cn/img/3_1.png)
![mashua](http://exam.wcpdoc.cn/img/3_2.png)
![mashua](http://exam.wcpdoc.cn/img/3_3.png)
![mashua](http://exam.wcpdoc.cn/img/3_4.png)
![mashua](http://exam.wcpdoc.cn/img/3_5.png)

## 后台

> ###试题分类管理-权限
> 
![mashua](http://exam.wcpdoc.cn/img/4.png)


## 测试
系统管理员	sysadmin	111111	系统管理员

张三		zs		zs	判卷用户

李四		ls		ls	判卷用户

王五		ww		ww	考试用户

赵六		zl		zl	考试用户

钱七		qq		qq	考试用户

孙八		sb		sb	考试用户

杨九		yj		yj	考试用户

吴十		ws		ws	考试用户



## 技术实现

bs架构，采用开源组件spring4.2.9、springmvc4.2.9、hibernate4.3.11、mysql5.5、jdk7、tomcat7、easyui1.4.5、bootstrap3.3.7